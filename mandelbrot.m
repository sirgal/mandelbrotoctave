function mandel_img = mandel ()
  %% Parameters
  w = [-2.5 1.5]; % Domain
  h = [-1.5 1.5]; % Range
  s = 0.005;      % Step size
  it = 64;        % Iteration depth

  %% Prepare the complex plane
  [wa ha] = meshgrid (w(1):s:w(2), h(1):s:h(2));
  complex_plane = wa + ha * i;

  %% Preallocate image
  mandel_img = zeros( length(h(1):s:h(2)), length(w(1):s:w(2)));

  %% Generate mandelbrot
  for wi = 1:size(mandel_img, 2)
    for hi = 1:size(mandel_img, 1)

      z = 0;
      k = 0;
      while k < it && abs(z) < 2
        z = z^2 + complex_plane (hi, wi);
        k = k + 1;
      end
      mandel_img (hi, wi) = k - 1;

    end
    %% Display progress
    waitbar (wi/size(mandel_img, 2));
  end
end

m = mandel();
cmap = [hot(63); 0 0 0];
imwrite(m + 1, cmap, "mandel.png");